package practice2

class PersonName(val firstName: String, val middleName: String?, val lastName: String) :
        Comparable<PersonName?> {
    override fun toString(): String {
        if (hasMiddle()) {
            return "$lastName $firstName $middleName"
        } else {
            return "$lastName $firstName"
        }
    }

    fun hasMiddle(): Boolean {
        return middleName != null
    }

    override fun compareTo(other: PersonName?): Int {
        if (other == null) {
            return Int.MAX_VALUE
        }
        val last = lastName.compareTo(other.lastName)
        if (last != 0) {
            return last
        }
        val first = firstName.compareTo(other.firstName)
        if (first != 0) {
            return first
        }
        if (middleName != null && other.middleName != null) {
            return middleName.compareTo(other.middleName)
        }
        return 0
    }
}

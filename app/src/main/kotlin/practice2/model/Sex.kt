package practice2

public enum class Sex {
    MALE {
        override val label = "МУЖ"
        override val retireAge = 65
    },
    FEMALE {
        override val label = "ЖЕН"
        override val retireAge = 60
    };

    abstract val label: String
    abstract val retireAge: Int
    
    override fun toString() = label
}

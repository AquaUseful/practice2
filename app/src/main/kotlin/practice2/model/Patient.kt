package practice2

import java.time.Instant
import java.time.ZoneId
import java.time.temporal.ChronoUnit
import java.util.Calendar
import java.util.Date

data class Patient(
        var name: PersonName? = null,
        var bday: Date? = null,
        var sex: Sex? = null,
        var visit: Int = 1,
        private var cityInternal: String? = null,
        private var diseaseInternal: String? = null,
) : Comparable<Patient> {

    val age: Int
        get() {
            var bdayInstant = bday?.toInstant() ?: return -1
            var now = Instant.now()
            return ChronoUnit.YEARS
                    .between(
                            bdayInstant.atZone(ZoneId.systemDefault()),
                            now.atZone(ZoneId.systemDefault())
                    )
                    .toInt()
        }

    val retired: Boolean
        get() {
            val retireAge = sex?.retireAge
            return (retireAge ?: Int.MAX_VALUE) <= age
        }

    var city: String
        get() = cityInternal ?: ""
        set(value) {
            cityInternal = value.lowercase()
        }

    var disease: String
        get() = diseaseInternal ?: ""
        set(value) {
            diseaseInternal = value.lowercase()
        }

    val firstName: String
        get() = name?.firstName ?: ""

    val middleName: String
        get() = name?.middleName ?: ""

    val lastName: String
        get() = name?.lastName ?: ""

    fun fullName(firstName: String, middleName: String, lastName: String) {
        name = PersonName(firstName, middleName, lastName)
    }

    fun nameWithoutMiddlename(firstName: String, lastName: String) {
        name = PersonName(firstName, null, lastName)
    }

    fun bithday(year: Int, month: Int, day: Int) {
        var calendar = Calendar.getInstance()
        calendar.set(year, month, day)
        bday = calendar.getTime()
    }

    private fun nameString(): String {
        return name?.toString() ?: ""
    }

    override fun toString(): String {
        return "Имя: ${nameString()}\tПол: ${sex}\tВозраст: ${age}\tДиагноз: ${disease}\tНомер посещения: ${visit}\tГород: ${city}"
    }

    override fun compareTo(other: Patient): Int {
        return name?.compareTo(other.name) ?: Int.MIN_VALUE
    }
}

fun patient(init: Patient.() -> Unit): Patient {
    val patient = Patient()
    patient.init()
    return patient
}

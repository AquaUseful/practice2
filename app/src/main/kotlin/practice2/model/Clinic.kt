package practice2

import kotlin.collections.setValue
import kotlin.text.lowercase

public class Clinic {    
    private val patients = mutableListOf<Patient>()

    operator fun Patient.unaryPlus() {
        patients.add(this)
    }

    operator fun plusAssign(patient: Patient) {
        patients.add(patient)
    }

    infix fun findByLastName(lastName: String): List<Patient> {
        return patients.filter { p -> p.lastName == lastName }.sorted()
    }

    infix fun findByDisease(disease: String): List<Patient> {
        val dl = disease.lowercase()
        return patients.filter { p -> p.disease == dl }.sorted()
    }

    infix fun retiredByCity(city: String): List<Patient> {
        val cl = city.lowercase();
        return patients.filter { 
            p -> p.retired && (p.city == cl)
         }.sorted()
    }

    override fun toString(): String {
        val sb = StringBuilder("Список пациентов:\n")
        patients.sorted().forEach {
            p -> sb.append(p.toString() + "\n")
        }
        return sb.toString()
    }
}

fun clinic(init: Clinic.() -> Unit): Clinic {
    val clinic = Clinic()
    clinic.init()
    return clinic
}
